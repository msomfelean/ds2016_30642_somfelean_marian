using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;


namespace RemotableObjects
{

	public class frmRServer : System.Windows.Forms.Form, IObserver
	{
		private System.Windows.Forms.TextBox textBox1;
		private MyRemotableObject remotableObject;

		private System.ComponentModel.Container components = null;

		public frmRServer()
		{

			InitializeComponent();
			remotableObject = new MyRemotableObject();

			//************************************* TCP *************************************//
			// using TCP protocol
			TcpChannel channel = new TcpChannel(8080);
			ChannelServices.RegisterChannel(channel);
			RemotingConfiguration.RegisterWellKnownServiceType(typeof(MyRemotableObject), "HelloWorld", WellKnownObjectMode.Singleton);
			//************************************* TCP *************************************//
			RemotableObjects.Cache.Attach(this);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(656, 246);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(656, 246);
			this.Controls.Add(this.textBox1);
			this.Name = "Form1";
			this.Text = "RemoteServer";
			this.ResumeLayout(false);

		}
		#endregion


		[STAThread]
		static void Main()
		{
			Application.Run(new frmRServer());
		}

		#region IObserver Members

		public void Notify(string text)
		{
			SetText(text);
			//textBox1.Text = text;
		}

		public double GetTax(Car car)
		{
			var sum = 0;
			if (car.EngineSize < 1600)
				sum = 8;
			else if (car.EngineSize > 1600 && car.EngineSize < 2000)
				sum = 18;
			else if (car.EngineSize > 1600 && car.EngineSize < 2000)
				sum = 72;
			else if (car.EngineSize > 1600 && car.EngineSize < 2000)
				sum = 144;
			else if (car.EngineSize > 3001 )
				sum = 290;

			double tax = (car.EngineSize / 200) * sum;

			return tax;
		}

		public double GetSellingPrice(Car car)
		{
			return car.Price - (car.Price / 7) * (2015 - car.Year);
		}

		delegate void SetTextCallback(string text);
		private void SetText(string text)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (this.textBox1.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetText);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.textBox1.Text = text;
			}
		}
		#endregion
	}
}
