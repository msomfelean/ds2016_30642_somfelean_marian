using System;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;

namespace RemotableObjects
{

	public class MyRemotableObject : MarshalByRefObject
	{

		public MyRemotableObject()
		{

		}

		public double GetSellingPrice(Car car)
		{
			return Cache.GetInstance().GetSellingPrice(car);
		}

		public double GetTax(Car car)
		{
			return Cache.GetInstance().GetTax(car);
		}


	}
}
