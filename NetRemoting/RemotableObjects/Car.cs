﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RemotableObjects
{
	[Serializable]
	public class Car
	{
		public int Year { get; set; }
		public int EngineSize { get; set; }
		public double Price { get; set; }
	}
}
