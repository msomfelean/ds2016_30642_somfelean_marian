using System;

namespace RemotableObjects
{

	public interface IObserver
	{
		void Notify(string text);
		double GetTax(Car car);
		double GetSellingPrice(Car car);
	}

}
