using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace RemotableObjects
{


	public class frmRCleint : System.Windows.Forms.Form
	{

		MyRemotableObject remoteObject;
		private Label label1;
		private Label label2;
		private Label label3;
		private TextBox txtYear;
		private TextBox txtSize;
		private TextBox txtPrice;
		private Button btnSubmit;
		private GroupBox boxOutput;

		private System.ComponentModel.Container components = null;

		public frmRCleint()
		{

			InitializeComponent();

			//************************************* TCP *************************************//
			// using TCP protocol
			// running both client and server on same machines
			TcpChannel chan = new TcpChannel();
			ChannelServices.RegisterChannel(chan);
			// Create an instance of the remote object
			remoteObject = (MyRemotableObject)Activator.GetObject(typeof(MyRemotableObject), "tcp://localhost:8080/HelloWorld");
			// if remote object is on another machine the name of the machine should be used instead of localhost.
			//************************************* TCP *************************************//
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtYear = new System.Windows.Forms.TextBox();
			this.txtSize = new System.Windows.Forms.TextBox();
			this.txtPrice = new System.Windows.Forms.TextBox();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.boxOutput = new System.Windows.Forms.GroupBox();
			this.SuspendLayout();
		
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(82, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Fabrication year";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Engine size";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 70);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(86, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Purchasing price";
			// 
			// txtYear
			// 
			this.txtYear.Location = new System.Drawing.Point(121, 13);
			this.txtYear.Name = "txtYear";
			this.txtYear.Size = new System.Drawing.Size(100, 20);
			this.txtYear.TabIndex = 4;
			// 
			// txtSize
			// 
			this.txtSize.Location = new System.Drawing.Point(121, 41);
			this.txtSize.Name = "txtSize";
			this.txtSize.Size = new System.Drawing.Size(100, 20);
			this.txtSize.TabIndex = 5;
			// 
			// txtPrice
			// 
			this.txtPrice.Location = new System.Drawing.Point(121, 67);
			this.txtPrice.Name = "txtPrice";
			this.txtPrice.Size = new System.Drawing.Size(100, 20);
			this.txtPrice.TabIndex = 6;
			// 
			// btnSubmit
			// 
			this.btnSubmit.Location = new System.Drawing.Point(121, 93);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(75, 23);
			this.btnSubmit.TabIndex = 8;
			this.btnSubmit.Text = "Submit";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// boxOutput
			// 
			this.boxOutput.Location = new System.Drawing.Point(16, 149);
			this.boxOutput.Name = "boxOutput";
			this.boxOutput.Size = new System.Drawing.Size(200, 100);
			this.boxOutput.TabIndex = 9;
			this.boxOutput.TabStop = false;
			// 
			// frmRCleint
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(611, 296);
			this.Controls.Add(this.boxOutput);
			this.Controls.Add(this.btnSubmit);
			this.Controls.Add(this.txtPrice);
			this.Controls.Add(this.txtSize);
			this.Controls.Add(this.txtYear);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "frmRCleint";
			this.Text = "RemoteClient";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion


		[STAThread]
		static void Main()
		{
			Application.Run(new frmRCleint());
		}
		
		private void btnSubmit_Click(object sender, EventArgs e)
		{
			var car = new Car { EngineSize = Convert.ToInt16(txtSize.Text), Year = Convert.ToInt16(txtYear.Text), Price = Convert.ToDouble(txtPrice.Text) };
			var tax = remoteObject.GetTax(car);
			var sellingPrice = remoteObject.GetSellingPrice(car);

			boxOutput.Text = string.Format("Car tax:{0}. Selling price: {1}", tax, sellingPrice);
		}


	}
}
