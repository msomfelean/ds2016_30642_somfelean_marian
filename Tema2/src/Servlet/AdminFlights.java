
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import DataAceess.FlightDao;
import Model.Flight;
import java.util.List;

/**
 * Servlet implementation class UserFlights
 */
@WebServlet("/AdminFlights")
public class AdminFlights extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HttpSession session;
	 private FlightDao flightDao;
	 public AdminFlights() {
	        // TODO Auto-generated constructor stub
	    	Configuration configuration = new Configuration().configure("/resources/hibernate.cfg.xml");
	    	flightDao = new FlightDao(configuration.buildSessionFactory());
	    	
	    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);
		if(session.getAttribute("UserType") == null || !session.getAttribute("UserType").equals("1") )
		{
			response.sendRedirect("Login");
		}
		// 	String method = request.getMethod();
		// TODO Auto-generated method stub
		List<Flight> flights =  flightDao.getAll();
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String html = "";
		html +="<html><body>";
		html +="<h1>Admin Flights</h1>";
		html +="<label>Start date</label> <input type='text' name='StartDate' /> <br/> <br/>";
		html +="<label>End date</label><input type='text' name='EndDateDate' />";
		html += "<table>";
		if(flights !=null){
			html += "<thead><tr><td>Flight number</td><td>Plane type</td><td>Departure city</td><td>Departure time</td><td>Arival city</td><td>Arival date</td></tr></thead>";
			for (int i =0;i<flights.size();i++)
			{
				html+= "<tr>";
				html+="<td>"+flights.get(i).getFlightNumber()+"</td>";
				html+="<td>"+flights.get(i).getAirplaneType()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureCity()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureDate()  +"</td>";
				html+="<td>"+flights.get(i).getArrivalCity()  +"</td>";
				html+="<td>"+flights.get(i).getArivateDate()  +"</td>";
				html+= "</tr>";
			}
		}
		
		html +="</table></body></html>";
		
		out.print(html);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);
		if(session.getAttribute("UserType") == null ||session.getAttribute("UserType") =="2")
		{
			RequestDispatcher dispatcher = request.getRequestDispatcher("Login");
			dispatcher.forward(request, response);
		}
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}


