package Servlet;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import DataAceess.FlightDao;
import Model.Flight;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Servlet implementation class UserFlights
 */
@WebServlet("/UserFlights")
public class UserFlights extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HttpSession session;
	 private FlightDao flightDao;
	 public UserFlights() {
	        // TODO Auto-generated constructor stub
	    	Configuration configuration = new Configuration().configure("/resources/hibernate.cfg.xml");
	    	flightDao = new FlightDao(configuration.buildSessionFactory());
	    	
	    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);
		int userType =0;
		if(session.getAttribute("UserType") !=null)
		     userType =(int)session.getAttribute("UserType");
		
		if(userType == 0){
			response.sendRedirect("Login");
			return;
		}
				
		// 	String method = request.getMethod();
		// TODO Auto-generated method stub
		List<Flight> flights =  flightDao.getAll();
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String html = "";
		html +="<html><body>";
		html +="<h1>Flights</h1>";
		html +="<form action ='http://localhost:8080/SomfyAir/UserFlights' Method='POST'>";
		html +="<label>Start date</label> <input type='text' name='startDate' /> <br/> <br/>";
		html +="<label>End date</label><input type='text' name='endDate' /> <br/>";
		html += "<button type='submit' id='btnSubmit' Value='Submit'>Submit </button>  ";
		html += "<form>";
		html += "<table>";
		if(flights !=null){
			
			if(userType ==1)
			{
				html += "<thead><tr><td>Id</td><td>Flight number</td><td>Plane type</td><td>Departure city</td><td>Departure time</td><td>Arival city</td><td>Arival date</td><td>Operations</td></tr></thead>";
			}
			else{
				html += "<thead><tr><td>Flight number</td><td>Plane type</td><td>Departure city</td><td>Departure time</td><td>Arival city</td><td>Arival date</td></tr></thead>";
			}
			
		
			for (int i =0;i<flights.size();i++)
			{
				html+= "<tr>";
				if(userType ==1){
				html+="<td>"+flights.get(i).getId()+"</td>";
				}
				html+="<td>"+flights.get(i).getFlightNumber()+"</td>";
				html+="<td>"+flights.get(i).getAirplaneType()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureCity()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureDate()  +"</td>";
				html+="<td>"+flights.get(i).getArrivalCity()  +"</td>";
				html+="<td>"+flights.get(i).getArivateDate()  +"</td>";
				if(userType ==1){
				html+="<td><a href ='http://localhost:8080/SomfyAir/ManageFlight?id="+flights.get(i).getId()+"'>Edit</a><br/> ";
				html+="</td>";
				}
				html+= "</tr>";
			}
		}
		
		html+="</table>";
		if(userType ==1)
		{
			html +="<a href='http://localhost:8080/SomfyAir/ManageFlight'>Add new flight <a>";
		}
		else{
		html +="</body></html>";
		}
		
		out.print(html);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		session = request.getSession(true);
		int userType =0;
		if(session.getAttribute("UserType") !=null)
		     userType =(int)session.getAttribute("UserType");
		
		if(userType == 0){
			response.sendRedirect("Login");
			return;
		}
		
		long difference = getDifference(new Date());
		String sd = (String) request.getParameter("startDate");
		String ed = (String) request.getParameter("endDate");
		Date startDate = new Date();
		Date endDate = new Date();
		DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
		try {
			
			startDate =df2.parse(sd);
			endDate =df2.parse(ed);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		URL url = new URL("https://maps.googleapis.com/maps/api/timezone/json?location=46.7834818,23.546473&timestamp="+difference);
		 HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		 connection.setRequestMethod("GET");
		//https://maps.googleapis.com/maps/api/timezone/json?location=46.7834818,23.546473&timestamp=1458000000
		// TODO Auto-generated method stub
		 			    //Get Response  
		
		        InputStream is = connection.getInputStream();
		 		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			    StringBuilder responseApi = new StringBuilder(); // or StringBuffer if Java version 5+
			    String line;
			    while ((line = rd.readLine()) != null) {
			    	responseApi.append(line);
			    	responseApi.append('\r');
			    }
			    rd.close();
			    
			    response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				String html = getResponse(userType, startDate, endDate,responseApi.toString());
			    out.print(html);
		//doGet(request, response);
	}
	private String getResponse(int userType,Date startDate,Date endDate,String extraHtml ){
	
		List<Flight> flights =  flightDao.getAll(startDate,endDate);
		

		String html = "";
		html +="<html><body>";
		html +="<h1>Flights</h1>";
		html +="<form action ='http://localhost:8080/SomfyAir/UserFlights' Method='POST'>";
		html +="<label>Start date</label> <input type='text' name='StartDate' /> <br/> <br/>";
		html +="<label>End date</label><input type='text' name='EndDateDate' />";
		html += "<button type='submit' id='btnSubmit' Value='Submit'>Submit </button>  ";
		html += "<form>";
		html += "<table>";
		if(flights !=null){
			
			if(userType ==1)
			{
				html += "<thead><tr><td>Id</td><td>Flight number</td><td>Plane type</td><td>Departure city</td><td>Departure time</td><td>Arival city</td><td>Arival date</td><td>Operations</td></tr></thead>";
			}
			else{
				html += "<thead><tr><td>Flight number</td><td>Plane type</td><td>Departure city</td><td>Departure time</td><td>Arival city</td><td>Arival date</td></tr></thead>";
			}
			
		
			for (int i =0;i<flights.size();i++)
			{
				html+= "<tr>";
				if(userType ==1){
				html+="<td>"+flights.get(i).getId()+"</td>";
				}
				html+="<td>"+flights.get(i).getFlightNumber()+"</td>";
				html+="<td>"+flights.get(i).getAirplaneType()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureCity()  +"</td>";
				html+="<td>"+flights.get(i).getDepartureDate()  +"</td>";
				html+="<td>"+flights.get(i).getArrivalCity()  +"</td>";
				html+="<td>"+flights.get(i).getArivateDate()  +"</td>";
				if(userType ==1){
				html+="<td><a href ='http://localhost:8080/SomfyAir/ManageFlight?id="+flights.get(i).getId()+"'>Edit</a><br/> ";
				html+="</td>";
				}
				html+= "</tr>";
			}
		}
		html +="</table>";
		
		html+="<br/>Api call response:"+extraHtml;
		if(userType ==1)
		{
			html +="<br/><a href='http://localhost:8080/SomfyAir/ManageFlight'>Add new flight <a>";
		}
		else{
		html +="</body></html>";
		}
		
		return html;
	}
	
	public long getDifference(Date date)
	{
	    
	    Calendar c = Calendar.getInstance();
	    c.setTime(date);
	    long time = c.getTimeInMillis();
	    long curr = System.currentTimeMillis();
	    long diff = curr - time;    //Time difference in milliseconds
	    return diff/1000;
	}

}
