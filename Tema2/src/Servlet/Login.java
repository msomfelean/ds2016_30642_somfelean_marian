package Servlet;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import DataAceess.AccountDao;
import Model.Account;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 private AccountDao accountDao;
    /**
     * Default constructor. 
     */
    public Login() {
        // TODO Auto-generated constructor stub
    	Configuration configuration = new Configuration().configure("/resources/hibernate.cfg.xml");
    	
    	accountDao = new AccountDao(configuration.buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String html = "";
		html +="<html><body>";
		html +="<h1>Login</h1>";
		html += "<form action ='http://localhost:8080/SomfyAir/Login' Method='POST'>";
		html += "<input type='text' id='Email' name='Email' placeholder='Email'> <br/> ";
		html += "<input type='password' id='Password' name='Password' placeholder='Password'><br/><br/> ";
		html += "<button type='submit' id='btnSubmit' Value='Login'>Login </button>  ";
		html +="</form></body></html>";
		
		out.print(html);
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String email = (String) request.getParameter("Email");
		String password = (String) request.getParameter("Password");
		//doGet(request, response);
		
		Account account = accountDao.getAccount(email,password);
		if(account ==null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.print(account !=null);
		}
		else{
		// Create a session object if it is already not  created.
		HttpSession session = request.getSession(true);
		session.setAttribute("UserType",account.getType());
		response.sendRedirect("UserFlights");
			/*if (account.getType() ==1){
				response.sendRedirect("AdminFlights");
			}
			else if (account.getType()==2){
				response.sendRedirect("UserFlights");
							}
							*/
		}
		
	}
}
