package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import DataAceess.FlightDao;
import Model.Flight;

/**
 * Servlet implementation class Flight
 */
@WebServlet("/ManageFlight")
public class ManageFlight extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HttpSession session;
	 private FlightDao flightDao;
	 public ManageFlight() {
	        // TODO Auto-generated constructor stub
	    	Configuration configuration = new Configuration().configure("/resources/hibernate.cfg.xml");
	    	flightDao = new FlightDao(configuration.buildSessionFactory());
	    	
	    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		session = request.getSession(true);
		int userType =0;
		if(session.getAttribute("UserType") !=null)
		     userType =(int)session.getAttribute("UserType");
		
		if(userType == 0){
			response.sendRedirect("Login");
			return;
		}
		else if (userType ==2){
			response.sendRedirect("UserFlights");
			return;
		}
		String query =request.getParameter("id");
		int id =0;
		if(query !=null && query!="" && !query.equals(""))
		{
			id =Integer.parseInt(query);
		}
	 	
		Flight flight =  new Flight();
		if(id >0)
			flight = flightDao.get(id);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
		 
		String dTime ="01/01/2016 12:00";
		
		try{
		dTime =df.format(flight.getDepartureDate());
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		String aTime ="01/01/2016 12:00";
		try{
		aTime =df.format(flight.getArivateDate());
		}
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		String fn = flight.getFlightNumber() !=null ?flight.getFlightNumber() :"";
		String at = flight.getAirplaneType() !=null ?flight.getAirplaneType() :"";
		String dc = flight.getDepartureCity() !=null ?flight.getDepartureCity() :"";
		String ac = flight.getArrivalCity() !=null ?flight.getArrivalCity() :"";
		
		String html = "";
		html +="<html><body>";
		html +="<h1>Manage flight</h1>";
		html += "<form action ='http://localhost:8080/SomfyAir/ManageFlight' Method='POST'>";
		html +="<input type='hidden' name='id' value='"+flight.getId()+"' /> <br/> <br/>";
		html +="<label>Flight number</label> <input type='text' name='FlightNumber' value='"+fn+"' /> <br/> <br/>";
		html +="<label>Airplane type</label><input type='text' name='AirplaneType' value='"+at+"' /> <br/> <br/> ";
		html +="<label>Departure city </label><input type='text' name='DepartureCity' value='"+dc+"' />  <br/> <br/>";
		html +="<label>Departure date</label><input type='text' name='DepartureDate' value='"+dTime+"'  /> <br/> <br/>";
		html +="<label>Arrival city</label><input type='text' name='ArrivalCity' value='"+ac+"'  /> <br/> <br/>";
		html +="<label>Arrival date</label><input type='text' name='ArrivalDate' value='"+aTime+"'  /> <br/> <br/>";
		html += "<button type='submit' id='btnSubmit' Value='Submit'>Submit </button>  ";
		html += "</form>";
		
		if(flight.getId() >0){
			html += "<form action ='http://localhost:8080/SomfyAir/ManageFlight' Method='POST'>";
			html +="<input type='hidden' name='id' value='"+flight.getId()+"' />";
			html+="<input type='hidden' name='action' value='delete'/>";
			html += "<button type='submit' id='btnSubmit' Value='Delete' style='color:white;background-color:red;'>Delete </button>  ";
			html += "</form>";
		}
		html +="</body></html>";
		
		out.print(html);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		session = request.getSession(true);
		int userType =0;
		if(session.getAttribute("UserType") !=null)
		     userType =(int)session.getAttribute("UserType");
		
		if(userType == 0){
			response.sendRedirect("Login");
			return;
		}
		else if (userType ==2){
			response.sendRedirect("UserFlights");
			return;
		}
		
		if("delete".equals((String)request.getParameter("action")))
		{
			String query =request.getParameter("id");
			int id =0;
			if(query !=null && query!="" && !query.equals(""))
			{
				id =Integer.parseInt(query);
			}
		 	
			if(id ==0)
			{
				response.sendRedirect("UserFlights");
				return;
			}
			flightDao.Delete(id);
			response.sendRedirect("UserFlights");
		}
		else{
		DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
		 
		int id = Integer.parseInt(request.getParameter("id"));
		String flightNumber = (String) request.getParameter("FlightNumber");
		String airplaneType = (String) request.getParameter("AirplaneType");
		String departureCity = (String) request.getParameter("DepartureCity");
		String departureDate = (String) request.getParameter("DepartureDate");
		String arrivalCity = (String) request.getParameter("ArrivalCity");
		String arrivalDate = (String) request.getParameter("ArrivalDate");
		
		Flight flight = new Flight();
		flight.setId(id);
		flight.setFlightNumber(flightNumber);
		flight.setAirplaneType(airplaneType);
		flight.setDepartureCity(departureCity);
		
		try {
			flight.setDepartureDate(df2.parse(departureDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		flight.setArrivalCity(arrivalCity);
		
		try {
			flight.setArivateDate(df2.parse(arrivalDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(id >0)
		    flightDao.Update(flight);
		else
			flightDao.Add(flight);
		
		response.sendRedirect("UserFlights");
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(true);
		int userType =0;
		if(session.getAttribute("UserType") !=null)
		     userType =(int)session.getAttribute("UserType");
		
		if(userType == 0){
			response.sendRedirect("Login");
			return;
		}
		else if (userType ==2){
			response.sendRedirect("UserFlights");
			return;
		}
	
	}

}
