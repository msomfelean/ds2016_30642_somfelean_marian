package Model;

import java.util.Date;

public class Flight {
	private int Id;
	private String FlightNumber;
	private String AirplaneType;
	private String DepartureCity;
	private Date DepartureDate;
	private String ArrivalCity;
	private Date ArivateDate;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFlightNumber() {
		return FlightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		FlightNumber = flightNumber;
	}
	public String getAirplaneType() {
		return AirplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		AirplaneType = airplaneType;
	}
	public String getDepartureCity() {
		return DepartureCity;
	}
	public void setDepartureCity(String departureCity) {
		DepartureCity = departureCity;
	}
	public Date getDepartureDate() {
		return DepartureDate;
	}
	public void setDepartureDate(Date departureDate) {
		DepartureDate = departureDate;
	}
	public String getArrivalCity() {
		return ArrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		ArrivalCity = arrivalCity;
	}
	public Date getArivateDate() {
		return ArivateDate;
	}
	public void setArivateDate(Date arivateDate) {
		ArivateDate = arivateDate;
	}
}
