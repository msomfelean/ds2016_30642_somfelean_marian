package DataAceess;

import org.hibernate.*;

import Model.Account;

import java.util.List;

public class AccountDao {
//	private static final Log LOGGER = LogFactory.getLog(AccountDao.class);

	private SessionFactory factory;
	
	public AccountDao(SessionFactory factory) {
		this.factory = factory;
	}
	public Account getAccount(String email,String password) {
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Account> accounts = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Account WHERE Email = :email and Password =:password");
			query.setParameter("email", email);
			query.setParameter("password", password);
			accounts = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return accounts != null && !accounts.isEmpty() ? accounts.get(0) : null;
		
	}
	
}

