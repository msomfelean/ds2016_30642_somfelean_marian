package DataAceess;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import Model.Flight;

public class FlightDao {
	private SessionFactory factory;
	
	public FlightDao(SessionFactory factory) {
		this.factory = factory;
	}
	
	public Flight Add(Flight flight) {
		int flightId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			flightId = (Integer) session.save(flight);
			flight.setId(flightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}
	
	public Flight Update(Flight flight) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}
	
	public void Delete(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("DELETE FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			query.executeUpdate();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	
	}
	
	public Flight get(int id) {
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> model = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			model = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return model != null && !model.isEmpty() ? model.get(0) : null;
		
	}
	public List<Flight> getAll() {
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> model = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			model = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return model;
		
	}
	public List<Flight> getAll(Date startDate,Date endDate) {
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> model = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight where ArivateDate >= :startDate and ArivateDate <= :endDate");
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
			model = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return model;
		
	}
}
