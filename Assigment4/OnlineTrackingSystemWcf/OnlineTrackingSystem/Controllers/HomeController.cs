﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineTrackingSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserService.UserServiceClient _userService;
        public HomeController()
        {
            _userService = new UserService.UserServiceClient();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UserService.User model)
        {
            try
            {
                _userService.Login(model.Email, model.Password);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);
        }

        public ActionResult Register()
        {
            ViewBag.Message = "Your application description page.";

            return View(new UserService.User());
        }

        [HttpPost]
        public ActionResult Register(UserService.User model)
        {
            try
            {
                _userService.Register(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }
    }
}