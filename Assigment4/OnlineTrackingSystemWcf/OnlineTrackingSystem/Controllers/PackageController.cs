﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineTrackingSystem.Controllers
{
    public class PackageController : Controller
    {
        private readonly PackageService.PackageServiceClient _packageService;
        public PackageController()
        {
            _packageService = new PackageService.PackageServiceClient();
        }

        public ActionResult Index()
        {
            try
            {
                var models = _packageService.GetAll();

                return View(models.Data);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult Manage(int packageId)
        {
            try
            {
                if (packageId > 0)
                {
                    var model = _packageService.Get(packageId);
                    return View(model.Data);
                }
                else
                    return View(new PackageService.Package());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Manage(PackageService.Package model)
        {
            try
            {
                if (model.Id > 0)
                    _packageService.Update(model);
                else
                    _packageService.Add(model);

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult Delete(int packageId)
        {
            try
            {
                 _packageService.Delete(packageId);

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

    }
}