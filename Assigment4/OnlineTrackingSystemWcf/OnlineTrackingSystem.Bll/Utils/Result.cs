﻿using OnlineTrackingSystem.Dal.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OnlineTrackingSystem.Bll
{
	/// <summary>
	/// Status for the result of an operation
	/// </summary>
	[DataContract]
	[Serializable]
	public enum ResultStatus
	{
		[EnumMemberAttribute]
		EXCEPTION = 500,
		[EnumMemberAttribute]
		ERROR = 502,
		[EnumMemberAttribute]
		OK = 0,
		[EnumMemberAttribute]
		WARNING = 1,
		[EnumMemberAttribute]
		INVALID_PRECOND = -3,
		[EnumMemberAttribute]
		UNPROCESSABLE_ENTITY = 422,

		[EnumMemberAttribute]
		NOT_FOUND = 60,
		[EnumMemberAttribute]
		AXAPTA_FOUND_ONLY = 62,

		[EnumMemberAttribute]
		PRICE_ZERO = 120,
		[EnumMemberAttribute]
		FAILED_TO_ADD = 121,
		[EnumMemberAttribute]
		EMPTY = 122,

		[EnumMemberAttribute]
		ALREADYEXISTS = 123,
		[EnumMemberAttribute]
		INACTIVE = 124,
	}

	/// <summary>
	/// Result class to be used on non data retrieving methods
	/// </summary>
	[DataContract]
	[Serializable]
	[KnownType(typeof(List<User>))]
    [KnownType(typeof(List<Package>))]
    [KnownType(typeof(List<Route>))]
    [KnownType(typeof(User))]
    [KnownType(typeof(Package))]
    [KnownType(typeof(Route))]
    public class Result
	{
		#region Properties

		[DataMember]
		public ResultStatus Status { get; set; }
		[DataMember]
		public string Message { get; set; }
		[DataMember]
		public string StatusText { get; set; }

		[DataMember]
		public Object Data { get; set; }

		#endregion

		public Result(ResultStatus status)
		{
			Status = status;
			StatusText = Status.ToString();
		}
		public Result()
			: this(ResultStatus.OK)
		{
		}
		public Result(ResultStatus status, string message)
			: this(status)
		{
			Message = message;
		}

		public Result(ResultStatus status, string message, Object obj)
			: this(status)
		{
			Message = message;
			Data = obj;
		}

		public Result(Exception ex)
			: this(ResultStatus.EXCEPTION, ex.Message)
		{
		}

		public Result(Object obj)
			: this(ResultStatus.OK)
		{
			Data = obj;
		}
		public bool IsOk()
		{
			return Status == ResultStatus.OK;
		}

	}

	/// <summary>
	/// Template result for any operation
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	[DataContract(Name = "ResultOf{0}")]
	public class Result<T>
	{
		#region Properties

		[DataMember]
		public T Data { get; set; }
		[DataMember]
		public ResultStatus Status { get; set; }
		[DataMember]
		public string Message { get; set; }
		[DataMember]
		public string StatusText { get; set; }

		#endregion

		public Result(ResultStatus status)
		{
			Status = status;
			StatusText = Status.ToString();
		}

		public Result()
			: this(ResultStatus.OK)
		{
		}

		public Result(ResultStatus status, string message)
			: this(status)
		{
			Message = message;
		}

		public Result(ResultStatus status, string message, T data)
			: this(status, message)
		{
			Data = data;
		}

		public Result(T data)
			: this(ResultStatus.OK)
		{
			Data = data;
		}

		public Result(Exception ex)
			: this(ResultStatus.EXCEPTION, ex.Message)
		{
		}

		public bool IsOk()
		{
			if (this.Data == null && Status == ResultStatus.OK)
				this.Status = ResultStatus.EMPTY;

			return ResultStatus.OK == Status;
		}
	}
}
