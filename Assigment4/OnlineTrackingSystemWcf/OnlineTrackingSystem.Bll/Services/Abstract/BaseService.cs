﻿using OnlineTrackingSystem.Dal.Infrastructure;

namespace OnlineTrackingSystem.Bll.Services.Abstract
{
	public abstract class BaseService : IBaseService
	{
		#region Properties
		protected readonly IUnitOfWork unitOfWork;
		#endregion

		public BaseService(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		#region IBaseService Members
		public virtual Result SaveChanges()
		{
			//TODO CHECK ALL FUNCTIONS THAT SAVED SO WE DO NOT HAVE ERRORS ON DELIVERY
			unitOfWork.Commit();
			return new Result();
		}
		#endregion
	}
}
