﻿namespace OnlineTrackingSystem.Bll.Services.Abstract
{
	public interface IBaseService
	{
		Result SaveChanges();
	}
}
