﻿using OnlineTrackingSystem.Bll.Services.Abstract;
using OnlineTrackingSystem.Dal.Infrastructure;
using OnlineTrackingSystem.Dal.Models;
using OnlineTrackingSystem.Dal.Repositories.Base;
using System;

namespace OnlineTrackingSystem.Bll.Services
{
    public interface IUserService
    {
        Result Login(string email, string password);
        Result Register(User model);
        Result GetAll();
    }

    public class UserService : BaseService, IUserService
    {
        private readonly IEntityBaseRepository<User> _userRepository;

        public UserService(IEntityBaseRepository<User> userRepository, IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _userRepository = userRepository;
        }

        public Result Login(string email, string password)
        {
            var user = _userRepository.Get(p => p.Email == email && p.Password == password);
            if (user == null)
                return new Result(ResultStatus.ERROR, "Invalid username or password");

            return new Result(ResultStatus.OK,"Success",user);
        }

        public Result Register(User model)
        {
            _userRepository.Add(model);
            model.IsAdmin = false;
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success", model);
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }
        public Result GetAll()
        {
            var users = _userRepository.GetAll();
            if (users == null)
                return new Result(ResultStatus.ERROR, "Invalid username or password");

            return new Result(ResultStatus.OK, "Success", users);
        }
    }
}
