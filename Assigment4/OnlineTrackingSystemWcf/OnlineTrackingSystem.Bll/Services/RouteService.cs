﻿using OnlineTrackingSystem.Bll.Services.Abstract;
using OnlineTrackingSystem.Dal.Infrastructure;
using OnlineTrackingSystem.Dal.Models;
using OnlineTrackingSystem.Dal.Repositories.Base;
using System;

namespace OnlineTrackingSystem.Bll.Services
{
    public interface IRouteService
    {
        Result Add(Route model);
        Result Update(Route model);
        Result Delete(int id);
    }

    public class RouteService : BaseService, IRouteService
    {
        private readonly IEntityBaseRepository<Route> _routeRepository;

        public RouteService(IEntityBaseRepository<Route> routeRepository, IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _routeRepository = routeRepository;
        }

        public Result Add(Route model)
        {
            _routeRepository.Add(model);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success", model);
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

        public Result Update(Route model)
        {
            _routeRepository.Update(model);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success", model);
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

        public Result Delete(int id)
        {
            _routeRepository.Delete(p=>p.Id ==id);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success");
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

    }
}
