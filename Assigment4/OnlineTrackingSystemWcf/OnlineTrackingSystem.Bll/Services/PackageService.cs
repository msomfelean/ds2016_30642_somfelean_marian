﻿using System;
using OnlineTrackingSystem.Bll.Services.Abstract;
using OnlineTrackingSystem.Dal.Infrastructure;
using OnlineTrackingSystem.Dal.Models;
using OnlineTrackingSystem.Dal.Repositories.Base;
using System.Linq;
using System.Collections.Generic;

namespace OnlineTrackingSystem.Bll.Services
{
    public interface IPackageService
    {
        Result Add(Package model);

        Result Update(Package model);

        Result Delete(int packageId);

        Result GetAll(int id);

        Result Get(int id);
    }

    public class PackageService : BaseService, IPackageService
    {
        private readonly IEntityBaseRepository<Package> _packageRepository;
        private readonly IEntityBaseRepository<Route> _routeRepository;

        public PackageService(IEntityBaseRepository<Package> packageRepository, IUnitOfWork unitOfWork, IEntityBaseRepository<Route> routeRepository)
            : base(unitOfWork)
        {
            _packageRepository = packageRepository;
            _routeRepository = routeRepository;
        }

        public Result Add(Package model)
        {
            _packageRepository.Add(model);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success");
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

        public Result Delete(int packageId)
        {
            _routeRepository.Delete(p => p.PackageId == packageId);
            _packageRepository.Delete(p => p.Id == packageId);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success");
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

        public Result GetAll(int id)
        {
            var packages = new List<Package>();

            if (id > 0)
                packages = _packageRepository.FindByIncluding(p => p.SenderId == id || p.ReceiverId == id, p => p.Receiver, p => p.Sender).ToList();
            else
                packages = _packageRepository.GetAllIncluding(p => p.Receiver, p => p.Sender).ToList();

            if (packages == null)
                return new Result(ResultStatus.NOT_FOUND);

            return new Result(ResultStatus.OK, "Success", packages.ToList());
        }

        public Result Update(Package model)
        {
            _packageRepository.Update(model);
            try
            {
                SaveChanges();
                return new Result(ResultStatus.OK, "Success");
            }
            catch (Exception ex)
            {
                return new Result(ex);
            }
        }

        public Result Get(int id)
        {
            var dbModel = _packageRepository.GetByIncluding(p => p.Id == id, p => p.Routes);
            if (dbModel == null)
                return new Result(ResultStatus.ERROR, "Not found");

            return new Result(ResultStatus.OK, "Success", dbModel);
        }

    }

}
