﻿using OnlineTrackingSystem.Dal.Models;
using System.ServiceModel;
using OnlineTrackingSystem.Bll;

namespace OnlineTrackingSystemWcf.Services
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        Result Login(string email, string password);
        [OperationContract]
        Result Register(User user);

        [OperationContract]
        Result GetAll();
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "User" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select User.svc or User.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUserService
    {
        private readonly OnlineTrackingSystem.Bll.Services.IUserService _userService;

        public UserService(OnlineTrackingSystem.Bll.Services.IUserService userService)
        {
            _userService = userService;
        }

        public Result Login(string email, string password)
        {
            return _userService.Login(email, password);
        }

        public Result Register(User user)
        {
            return _userService.Register(user);
        }
        public Result GetAll()
        {
            return _userService.GetAll();
        }
    }

}
