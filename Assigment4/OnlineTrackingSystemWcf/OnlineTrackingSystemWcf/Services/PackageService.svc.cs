﻿using OnlineTrackingSystem.Bll;
using OnlineTrackingSystem.Dal.Models;

using System.ServiceModel;

namespace OnlineTrackingSystemWcf.Services
{
    [ServiceContract]
    public interface IPackageService
    {
        [OperationContract]
        Result Add(Package model);

        [OperationContract]
        Result Update(Package model);

        [OperationContract]
        Result Delete(int packageId);

        [OperationContract]
        Result GetAll(int id = 0);

        [OperationContract]
        Result Get(int id);

        [OperationContract]
        Result AddRoute(Route model);

        [OperationContract]
        Result UpdateRoute(Route model);

        [OperationContract]
        Result DeleteRoute(int id);
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PackageService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PackageService.svc or PackageService.svc.cs at the Solution Explorer and start debugging.
    public class PackageService : IPackageService
    {
        private readonly OnlineTrackingSystem.Bll.Services.IPackageService _packageService;
        private readonly OnlineTrackingSystem.Bll.Services.IRouteService _routeService;

        public PackageService(OnlineTrackingSystem.Bll.Services.IPackageService packageService, OnlineTrackingSystem.Bll.Services.IRouteService routeService)
        {
            _packageService = packageService;
            _routeService = routeService;
        }

        public Result AddRoute(Route model)
        {
            return _routeService.Add(model);
        }
        public Result UpdateRoute(Route model)
        {
            return _routeService.Update(model);
        }
        public Result DeleteRoute(int id)
        {
            return _routeService.Delete(id);
        }

        public Result Add(Package model)
        {
            return _packageService.Add(model);
        }

        public Result Delete(int packageId)
        {
            return _packageService.Delete(packageId);
        }

        public Result GetAll(int id = 0)
        {
            return _packageService.GetAll(id);
        }

        public Result Get(int id)
        {
            return _packageService.Get(id);
        }

        public Result Update(Package model)
        {
            return _packageService.Update(model);
        }
    }
}
