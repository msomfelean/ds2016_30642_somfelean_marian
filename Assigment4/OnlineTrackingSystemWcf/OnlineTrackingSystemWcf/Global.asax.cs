﻿using Autofac;
using Autofac.Integration.Wcf;
using OnlineTrackingSystemWcf.Services;
using OnlineTrackingSystem.Dal.Infrastructure;
using OnlineTrackingSystem.Dal.Repositories.Base;
using System;
using System.ServiceModel.Activation;
using System.Web.Routing;

namespace OnlineTrackingSystemWcf
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            // Register your service implementations.
            builder.RegisterType<UserService>();
            builder.RegisterType<PackageService>();


            builder.RegisterAssemblyTypes(typeof(OnlineTrackingSystem.Bll.Services.UserService).Assembly)
       .Where(t => t.Name.EndsWith("Service"))
       .AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerLifetimeScope();



            builder.RegisterGeneric(typeof(EntityBaseRepository<>))
            .As(typeof(IEntityBaseRepository<>))
            .InstancePerLifetimeScope();

            // Set the dependency resolver.
            var container = builder.Build();
            AutofacHostFactory.Container = container;

            RouteTable.Routes.Add(new ServiceRoute("UserService", new AutofacServiceHostFactory(), typeof(UserService)));
            RouteTable.Routes.Add(new ServiceRoute("PackageService", new AutofacServiceHostFactory(), typeof(PackageService)));
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}