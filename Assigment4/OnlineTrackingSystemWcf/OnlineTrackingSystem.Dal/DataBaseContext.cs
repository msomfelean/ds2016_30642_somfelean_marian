﻿using OnlineTrackingSystem.Dal.Models;
using System.Data.Entity;

namespace OnlineTrackingSystem.Dal
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class DataBaseContext : DbContext
    {
         public DataBaseContext()
            : base("DatabaseContext")
        {

            Database.SetInitializer<DataBaseContext>(null);
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual void Commit()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().HasMany(q => q.ReceivedPackages).WithRequired(p => p.Receiver).HasForeignKey(q => q.ReceiverId).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(q => q.SendPackages).WithRequired(p => p.Sender).HasForeignKey(q => q.SenderId).WillCascadeOnDelete(false);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Route> Routes { get; set; }

    }
}
