﻿using OnlineTrackingSystem.Dal.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;

namespace OnlineTrackingSystem.Dal.Repositories.Base
{
	public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, new()
	{
		#region Properties
		private DataBaseContext _dataContext;
		private readonly IDbSet<T> _dbSet;

		protected IDbFactory DbFactory
		{
			get;
			private set;
		}

		protected DataBaseContext DbContext
		{
			get { return _dataContext ?? (_dataContext = DbFactory.Init()); }
		}
		#endregion

		public EntityBaseRepository(IDbFactory dbFactory)
		{
			DbFactory = dbFactory;
			_dbSet = DbContext.Set<T>();
		}

		#region IEntityBaseRepository Implementation
		public virtual void Add(T entity)
		{
			_dbSet.Add(entity);
		}

		public virtual void AddBatch(IEnumerable<T> entities)
		{
			foreach (var entity in entities)
				_dbSet.Add(entity);
		}

		public virtual void Update(T entity)
		{
			_dbSet.Attach(entity);
			_dataContext.Entry(entity).State = EntityState.Modified;
		}

		public void UpdateBatch(IEnumerable<T> entities)
		{

			foreach (var entity in entities)
			{
				_dbSet.Attach(entity);
				_dataContext.Entry(entity).State = EntityState.Modified;
			}
		}

		public virtual void Delete(T entity)
		{
			_dbSet.Remove(entity);
		}

		public virtual void Delete(Expression<Func<T, bool>> predicate)
		{
			IEnumerable<T> objects = _dbSet.Where(predicate).AsEnumerable();
			foreach (var obj in objects)
			{
				_dbSet.Remove(obj);
			}
		}

		public virtual T GetById(int id)
		{
			return _dbSet.Find(id);
		}

		public virtual T GetByIncluding(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
		{
			IQueryable<T> query = _dbSet;
			foreach (var includeProperty in includeProperties)
			{
				query = query.Include(includeProperty);
			}
			return query.AsExpandable().FirstOrDefault(predicate);
		}

		public virtual T Get(Expression<Func<T, bool>> predicate)
		{
			return _dbSet.Where(predicate).FirstOrDefault();
		}

		public virtual IEnumerable<T> GetAll()
		{

			return _dbSet.ToList();
		}

		public virtual IEnumerable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
		{
			IQueryable<T> query = _dbSet;
			if (includeProperties != null)
				foreach (var includeProperty in includeProperties)
				{
					query = query.Include(includeProperty);
				}

			this._dataContext.Configuration.ProxyCreationEnabled = false;
			query.Load();
			this._dataContext.Configuration.ProxyCreationEnabled = true;

			return query.ToList();
		}

		public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
		{
			return _dbSet.AsExpandable().Where(predicate).ToList();
		}

		public virtual IEnumerable<T> FindByIncluding(Expression<Func<T, bool>> predicate, Func<T, object> orderBy, bool ascending, int pageIndex, int pageSize, params Expression<Func<T, object>>[] includeProperties)
		{
			IQueryable<T> query = _dbSet;
			foreach (var includeProperty in includeProperties)
			{
				query = query.Include(includeProperty);
			}
			query = query.AsExpandable().Where(predicate);
			query = ascending ? query.OrderBy(orderBy).AsQueryable() : query.OrderByDescending(orderBy).AsQueryable();
			return PaginateSource(query, pageIndex, pageSize).ToList();
		}

		public virtual IEnumerable<T> FindByIncluding(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
		{
			IQueryable<T> query = _dbSet;
			foreach (var includeProperty in includeProperties)
			{
				query = query.Include(includeProperty);
			}
			query = query.AsExpandable().Where(predicate);
			return query.ToList();
		}
		public virtual int Count()
		{
			return _dbSet.Count();
		}

		public virtual int Count(Expression<Func<T, bool>> predicate)
		{
			return _dbSet.AsExpandable().Where(predicate).Count();
		}
		#endregion

		protected static IQueryable<T> PaginateSource(IQueryable<T> source, int pageIndex, int pageSize)
		{
			return (source.Skip((pageIndex - 1) * pageSize).Take(pageSize));
		}

		public void EnablePoco()
		{
			this._dataContext.Configuration.ProxyCreationEnabled = true;

		}
		public void DisablePoco()
		{
			this._dataContext.Configuration.ProxyCreationEnabled = false;
		}
	}
}
