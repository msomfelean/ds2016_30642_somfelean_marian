﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace OnlineTrackingSystem.Dal.Models
{
    [DataContract]
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public bool IsAdmin { get; set; }

        [DataMember]
        public virtual List<Package> SendPackages { get; set; }
        [DataMember]
        public virtual List<Package> ReceivedPackages { get; set; }
    }
}
