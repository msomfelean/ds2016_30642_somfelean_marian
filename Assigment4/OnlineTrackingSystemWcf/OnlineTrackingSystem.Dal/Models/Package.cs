﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace OnlineTrackingSystem.Dal.Models
{
    [DataContract]
    public class Package
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int SenderId { get; set; }
        [DataMember]
        public int ReceiverId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string SenderCity { get; set; }
        [DataMember]
        public string DestinationCity { get; set; }
        [DataMember]
        public bool Tracking { get; set; }

        [ForeignKey("SenderId")]
        public virtual User Sender { get; set; }

        [ForeignKey("ReceiverId")]
        public virtual User Receiver { get; set; }

        [DataMember]
        public virtual List<Route> Routes { get; set; }
    }
}
