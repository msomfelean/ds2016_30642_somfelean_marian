﻿using System;

namespace OnlineTrackingSystem.Dal.Infrastructure
{
    public interface IDbFactory
    {
        DataBaseContext Init();
    }
}
