﻿using System;

namespace OnlineTrackingSystem.Dal.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
