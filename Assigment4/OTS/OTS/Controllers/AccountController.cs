﻿using OTS.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OTS.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserServiceClient _userService;
        public AccountController()
        {
            _userService = new UserServiceClient();
        }

        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Login(User model)
        {
            try
            {
                var login = _userService.Login(model.Email, model.Password);
                if (login.Status == ResultStatus.OK)
                {
                    Session["User"] = (User)login.Data;
                    if (((User)login.Data).IsAdmin)
                        return RedirectToAction("Index", "Package");
                    else
                        return RedirectToAction("IndexUser", "Package");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(model);
        }

        public ActionResult Register()
        {
            ViewBag.Message = "Your application description page.";

            return View(new User());
        }

        [HttpPost]
        public ActionResult Register(User model)
        {
            try
            {
                var register = _userService.Register(model);
                if (register.Status == ResultStatus.OK)
                {
                    Session["User"] = (User)register.Data;
                    return RedirectToAction("IndexUser", "Package");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

        public ActionResult Logoff()
        {
            Session["User"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}