﻿using OTS.PackageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineTrackingSystem.Controllers
{
    public class PackageController : Controller
    {
        private readonly PackageServiceClient _packageService;

        public PackageController()
        {
            _packageService = new PackageServiceClient();
        }

        public ActionResult Index()
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");

            try
            {
                var models = _packageService.GetAll(0);

                return View(models.Data);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult IndexUser()
        {
            if (Session["User"] == null || ((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");

            try
            {
                var models = _packageService.GetAll(((OTS.UserService.User)Session["User"]).Id);

                return View(models.Data);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult Manage(int id)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                if (id > 0)
                {
                    var model = _packageService.Get(id);
                    return View(model.Data);
                }
                else
                    return View(new Package());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Manage(Package model)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                if (model.Id > 0)
                    _packageService.Update(model);
                else
                    _packageService.Add(model);

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult ManageRoutes(int id)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                if (id > 0)
                {
                    var model = _packageService.Get(id);
                    return View(model.Data);
                }
                else
                    return View(new Package());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult AddRoute(int id)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");

            var route = new Route { PackageId = id };
            return View(route);
        }

        [HttpPost]
        public ActionResult AddRoute(Route model)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                _packageService.AddRoute(model);

                return RedirectToAction("ManageRoutes", new { id = model.PackageId });

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        public ActionResult DeleteRoute(int id, int packageId)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                _packageService.DeleteRoute(id);

                return RedirectToAction("ManageRoutes", new { id = packageId });

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Delete(int id)
        {
            if (Session["User"] == null || !((OTS.UserService.User)Session["User"]).IsAdmin)
                return RedirectToAction("Index", "Home");
            try
            {
                _packageService.Delete(id);

                return RedirectToAction("Index");

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

    }
}