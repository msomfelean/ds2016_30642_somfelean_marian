﻿using OTS.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineTrackingSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserServiceClient _userService;
        public HomeController()
        {
            _userService = new UserServiceClient();
        }

        public ActionResult Index()
        {
            
            return View();
        }
    }
}